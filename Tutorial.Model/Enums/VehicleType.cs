﻿namespace Tutorial.Model.Enums
{
    public enum VehicleType
    {
        Car = 0,
        Motorcycle = 1,
        Truck = 2,
        Van = 3
    }
}
