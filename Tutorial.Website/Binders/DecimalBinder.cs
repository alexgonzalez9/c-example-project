﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Tutorial.Web.Binders
{
    public class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            var modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                actualValue = Convert.ToDecimal(valueResult.AttemptedValue, CultureInfo.InvariantCulture);
            }
            catch
            {
                try
                {
                    actualValue = Convert.ToDecimal(valueResult.AttemptedValue.Replace('.', ','), CultureInfo.InvariantCulture);
                }
                catch
                {
                    try
                    {
                        actualValue = Convert.ToDecimal(valueResult.AttemptedValue.Replace(',', '.'), CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        modelState.Errors.Add(ex);
                    }
                }
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);

            return actualValue;
        }
    }
}
