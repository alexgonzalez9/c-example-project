﻿using AutoMapper;
using System;
using System.Web.Mvc;
using System.Web.Routing;
using Tutorial.DAL.Repositories;
using Tutorial.Logging;
using Tutorial.Web.Binders;
using Unity;
using Unity.Mvc5;

namespace Tutorial.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Logger.Log("Executing Application Start", LogLevel.DEBUG);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            MapperConfiguration mc = new MapperConfiguration(cfg =>
            {
                Config.MapperConfig.SetMapperConfig(cfg);
            });

            AreaRegistration.RegisterAllAreas();
            DependencyResolver.SetResolver(new UnityDependencyResolver(ResolveDependencies(mc)));
            Tutorial.Web.App_Start.RouteConfig.RegisterRoutes(RouteTable.Routes);

            ModelBinders.Binders.Add(typeof(Decimal), new DecimalModelBinder());

            Logger.Log("Finalizing Application Start", LogLevel.DEBUG);
        }


        private UnityContainer ResolveDependencies(MapperConfiguration mc)
        {
            UnityContainer container = new UnityContainer();

            container.RegisterType<IUnitOfWork, UnitOfWork>();

            container.RegisterType<IVehicleRepository, VehicleRepository>();
            container.RegisterType<IBrandRepository, BrandRepository>();
            container.RegisterType<IReparationRepository, ReparationRepository>();
            container.RegisterType<IWorkerRepository, WorkerRepository>();
            container.RegisterType<IModelRepository, ModelRepository>();

            container.RegisterInstance(mc.CreateMapper());

            return container;
        }
    }
}