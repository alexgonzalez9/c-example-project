﻿using System.Web.Mvc;
using Tutorial.Logging;

namespace Tutorial.Web.Controllers
{
    public class VehicleController : Controller
    {
        [Route("")]
        public ActionResult Login()
        {
            Logger.LogStatus("Calling default action", null, LogLevel.INFO);

            return View("~/Views/Vehicle/List.cshtml");
        }
    }
}